const express = require('express')
const { check, validationResult } = require('express-validator/check')
const { matchedData } = require('express-validator/filter')
const router = express.Router()

router.get('/', (req, res) => {
  res.render('index')
})

router.get('/form', (req, res) => {
  res.render('form', {
	data:{},
	errors:{},
	csrfToken: req.csrfToken(),
  })
})

router.post('/form-submission', [
  check('firstName')
    .isLength({ min:1 })
    .withMessage('firstName is required')
    .trim()
    .escape(),
  check('lastName')
    .isLength({ min:1 })
    .withMessage('lastName is required')
    .trim()
    .escape(),
  check('dob')
    .custom(value => {
	  return value.match(/\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01])$/)
	})
	.withMessage('Bad date of birth format should be yyyy-mm-dd'),
  check('email')
    .isEmail()
    .withMessage('Bad email')
    .normalizeEmail(),
  check('gender')
    .custom(value => {
	  return ['male', 'female'].indexOf(value) >= 0
	})
	.withMessage('gender should be either male or female'),
], (req, res) => {
  const errors = validationResult(req)
  console.log('ERRORS*********************')
  console.log(errors.mapped())
  res.render('form-submission', {
    data: req.body,
    errors: errors.mapped()
  })
  const data = matchedData(req)
  console.log('DATA**********************')
  console.log(data)
})

module.exports = router
